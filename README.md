# StockManager

Sample web application created with reactjs-firebase-e2e boilerplate (link comming soon). 
Created in the context of a project of master degree in computer science

This project support [ToolPad Tech](https://www.toolpad.fr/)
## Installation

Requirements : 
* [NodeJs 12.x.x](https://nodejs.org/en/)

Test-Requirements :
* [Chrome](https://www.google.com/intl/fr_fr/chrome/)
* [Selenium-Chrome Driver](https://chromedriver.chromium.org/downloads)

make sure to add the driver location in `$PATH`

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

Internet connection is require to use database !

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

### `yarn test`
Launches the test-unit and test-e2e use Chrome in Automated mode on URL test<br />

### `yarn test-unit`
Launches unit test use jest.config.unit.js, the unit test are located `./src/__test__/unit/`

### `yarn test-e2e`
Launches unit test use jest.config.e2e.js, the unit file: `./src/__test__/e2e/index.js`

The file: `./src/__test__/e2e/index.js` has been build use `./src/__test__/e2e/scripts/setup-test-file/setupTestFile.js`. 
This script build merge files which contained in `./src/__test__/e2e/conf/e2e.config.json`

A second file: should be executed before this test, it's : `./src/__test__/e2e/scripts/setup-test-firebase/setupTestFirebase`
*  use option `--clean` to clear Firebase Auth & Firebase Cloud Firestore (auth and database)
*  use option `--init` to init Firebase Auth & Firebase Cloud Firestore with data of file  : `./src/__test__/e2e/conf/firebase.content-test.json`

### Deployment

Deployment is delegete to [Firebase-CLI](https://firebase.google.com/docs/cli)

To deploy on your own Firebase project
*  Make sur to put the client access file (prod and test) for the app `./src/api/production.firebase.json`and `./src/api/test.firebase.json`
*  You have to put the Admin SDK (Firebase) credential here : `./src/__test__/e2e/conf/stockmanager-test-serviceAccountKey.json`
*  `firebase login`
*  `firebase use [your-project-id]` (`firebase projects:list`=>display projects list)
*  `firebase deploy`

## Contributing
This repo is not yet available for pull request....

## License
[MIT](https://choosealicense.com/licenses/mit/)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
