import React, {Component} from 'react';
import {MDBCard, MDBCardBody, MDBCol, MDBContainer, MDBRow} from "mdbreact";
import unallowedPng from "../assets/img/unallowed.png";

class UnAllowed extends Component{
    render() {
        const roles = this.props.match.params.permissionRequired.split('-');
        return(
            <MDBContainer fluid>
                <MDBRow center={true}>
                    <MDBCol size="2" className="mt-5">
                        <MDBCard>
                            <MDBCardBody>
                                <img src={unallowedPng} alt="unallowed-img" className="img-fluid"/>
                            </MDBCardBody>
                        </MDBCard>
                    </MDBCol>
                    <h3 className="mt-3">Vous ne pouvez pas accéder à cette fonctionnalité, rôle nécéssaire :</h3>
                    <ul>
                        {roles.map((role,index)=>{
                            return(
                                <li>{role==="director" ? "Directeur" : (role === "chefRayon" ? "Chef de ce rayon" : "L'utiliateur cible de l'action")}</li>
                            )
                        })}
                    </ul>
                </MDBRow>
            </MDBContainer>
        );
    }
}

export default UnAllowed;