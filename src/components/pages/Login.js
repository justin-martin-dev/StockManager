import React from 'react';
import {MDBBtn, MDBCard, MDBCardBody, MDBCol, MDBContainer, MDBIcon, MDBInput, MDBRow} from 'mdbreact';
import '../../assets/css/Login.css'
import Format from "../../utils/Format";
import FirebaseAPI from "../../api/FirebaseAPI";
import Swal from 'sweetalert2';

class Login extends React.Component {
    state = {
        email: "",
        password: ""
    };

    changeStateProperty = (property) => (event) => {
      this.setState({
        [property]: document.getElementById(`input-${property}-field`).value
      })
    };

    login = () => {
      const {email, password} = this.state;
      FirebaseAPI.getAuth()
          .signInWithEmailAndPassword(email, password)
          .then(() => {
              Swal.fire({
                  icon: 'success',
                  title: 'Vous êtes connecté',
                  text: 'Vous allez être redirigé dans quelques instants!',
                  timer: 1500
              });
          })
          .catch((error) => {
              Swal.fire({
                  icon: 'error',
                  title: 'Ops...',
                  text: `Il semblerait que vos "identifiant/mot de passe" sont faux! (error: ${error.code})`,
                  timer: 3000,
              });
          })
    };

    render() {
        return (
            <div id='login'>
                <MDBContainer>
                    <MDBRow center={true}>
                        <MDBCol md='10' lg='6' xl='5' sm='12' className='mt-5 mx-auto'>
                            <MDBCard color="white text-black">
                                <MDBCardBody>
                                    <div className='form-header purple-gradient'>
                                        <h3>
                                            <MDBIcon
                                                icon='user'
                                                className='mt-2 mb-2 text-white'
                                            />{' '}
                                            Log in:
                                        </h3>
                                    </div>
                                    <MDBInput
                                        type='email'
                                        value={this.state.email}
                                        onChange={this.changeStateProperty("email")}
                                        label='Adresse email'
                                        icon='envelope'
                                        id="input-email-field"
                                        className={this.state.email !== "" && Format.isEmail(this.state.email) ? "is-valid text-white" : "is-invalid text-white"}
                                        required
                                    >
                                      <div className='valid-feedback ml-4 pl-3'>Format email valide</div>
                                      <div className='invalid-feedback ml-4 pl-3'>Format email invalide</div>
                                    </MDBInput>
                                    <MDBInput
                                        type='password'
                                        onChange={this.changeStateProperty("password")}
                                        label='Mot de passe'
                                        icon='lock'
                                        id="input-password-field"
                                        className={this.state.password !== "" ? "is-valid text-white" : "is-invalid text-white"}
                                        required
                                    >
                                      <div className='valid-feedback ml-4 pl-3'>Format valide</div>
                                      <div className='invalid-feedback ml-4 pl-3'>Merci de rentrer un mot de passe</div>
                                    </MDBInput>
                                    <div className='text-center mt-3 black-text'>
                                        <MDBBtn className='purple-gradient' size='lg' onClick={this.login} disabled={this.state.password === "" || this.state.email === "" || !Format.isEmail(this.state.email)}>
                                            Login
                                        </MDBBtn>
                                        <hr/>
                                    </div>
                                </MDBCardBody>
                            </MDBCard>
                        </MDBCol>
                    </MDBRow>
                </MDBContainer>
            </div>
        );
    }
}

export default Login;
