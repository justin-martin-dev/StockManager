import React from 'react';
import { Route, Switch } from 'react-router-dom';
import {Redirect} from "react-router";

import StockDashboard from './dashboard/Stock/StockDashboard';
import AddEditUser from "./addEdit/AddEditUser";
import AddEditRayon from "./addEdit/AddEditRayon";
import AddEditArticle from "./addEdit/AddEditArticle";
import UserDashboard from "./dashboard/User/UserDashboard";
import UnAllowed from "./UnAllowed";

const fourtOFour = () => <h1 className="text-center">404</h1>;

class Routes extends React.Component {
    render() {
        //Mappage de l'application
        return (
            <Switch>
                <Route path='/' exact component={e=><Redirect to="/stock"/>} />
                <Route path='/stock' exact component={StockDashboard} />
                <Route path='/users' exact component={UserDashboard} />

                <Route path='/user/add' exact component={AddEditUser}/>
                <Route path='/user/edit/:userId' exact component={AddEditUser}/>

                <Route path='/rayon/add' exact component={AddEditRayon} />
                <Route path='/rayon/edit/:rayonId' exact component={AddEditRayon} />

                <Route path='/article/add' exact component={AddEditArticle} />
                <Route path='/article/edit/:articleId' exact component={AddEditArticle} />

                <Route path='/unallowed-page/:permissionRequired' exact component={UnAllowed} />
                <Route component={fourtOFour} />
            </Switch>
        );
    }
}

export default Routes;