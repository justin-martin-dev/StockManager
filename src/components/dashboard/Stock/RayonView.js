import React, {Component} from 'react';
import {
    MDBBadge, MDBBtn,
    MDBCard,
    MDBCardBody, MDBCol,
    MDBCollapse,
    MDBCollapseHeader,
    MDBIcon, MDBRow, MDBSpinner,
    MDBTable,
    MDBTableBody,
    MDBTableHead
} from "mdbreact";
import FirebaseAPI from "../../../api/FirebaseAPI";
import gifError from "../../../assets/img/error.gif";
import ArticleLine from "./ArticleLine";
import * as Swal from "sweetalert2";
import {withRouter} from "react-router";

class RayonView extends Component{
    state = {
        isOpen : true,
        loading: true,
        listArticleRef: []
    };

    toggleCollapse = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    };

    attachFirebaseQuery = () => {
        this.firebaseQuery = FirebaseAPI.getDatabase().collection("Article")
            .where("rayonId", "==", this.props.rayon.rayonId)
            .onSnapshot((docSnapshot)=>{
                console.log(this.props.rayon.nom, docSnapshot.docs);
                this.setState({
                    loading: false,
                    status: true,
                    isOpen: docSnapshot.docs.length > 0,
                    listArticleRef: docSnapshot.docs
                });
            }, (error)=>{
                this.setState({
                    loading: false,
                    status: false,
                    errorMessage: error.getMessage(),
                    errorCode: error.getCode()
                });
            })
    };

    componentDidMount() {
        this.attachFirebaseQuery();
    }

    async updateArticleRayon(doc){
        console.log("article to change", doc.data().nom);
        return FirebaseAPI.getDatabase().collection("Article").doc(doc.data().articleId).update({rayonId: null});
    }

    async updateUserRayon(doc){
        console.log("user to change", doc.data().prenom + " " + doc.data().nom);
        return FirebaseAPI.getDatabase().collection("User").doc(doc.data().userId).update({rayonId: null});
    }

    deleteRayon = () => {
        Swal.fire({
            title: 'Etes-vous sûr ?',
            text: `Les articles affectés à ce rayon seront déplacé dans le groupe "Non affecté" `,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui, le supprimer!'
        }).then((result) => {
            if(result.value) {
                this.firebaseQuery(); //detach database listener during query
                FirebaseAPI.getDatabase()
                    .collection("Article")
                    .where("rayonId", "==", this.props.rayon.rayonId)
                    .get()
                    .then(async (querySnapshot) => {
                        await Promise.all(querySnapshot.docs.map(this.updateArticleRayon));

                        FirebaseAPI.getDatabase()
                            .collection("User")
                            .where("rayonId", "==", this.props.rayon.rayonId)
                            .get()
                            .then(async (querySnapshot) => {
                                await Promise.all(querySnapshot.docs.map(this.updateUserRayon));

                                FirebaseAPI.getDatabase()
                                    .collection("Rayon")
                                    .doc(this.props.rayon.rayonId)
                                    .delete()
                                    .then(() => {
                                        this.attachFirebaseQuery();
                                        Swal.fire(
                                            'Supprimé!',
                                            `Le rayon ${this.props.rayon.nom} a été supprimé`,
                                            'success'
                                        )
                                    })
                                    .catch((error) => {
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Oops...',
                                            text: `Erreur : ${error.message}`,
                                        })
                                    });
                            })
                    });
            }
        });
    };

    render() {
        const {rayon} = this.props;
        const {isOpen, loading} = this.state;

        if(loading){
            return <MDBSpinner/>
        } else if(this.state.status) {
            return (
                <MDBCard className='mt-3'>
                    <MDBCollapseHeader
                        tagClassName='d-flex justify-content-between white-text'
                        onClick={this.toggleCollapse}
                    >
                        <div>
                            {rayon.nom}
                            <MDBBadge color="secondary" className="ml-2" id={`badge-nb-${rayon.rayonId}`}>{this.state.listArticleRef.length}</MDBBadge>
                        </div>
                        <div>
                            {rayon.rayonId !== null ? (
                                <>
                                    <MDBBtn tag="a" floating color="danger" className="mr-2 my-1" onClick={this.deleteRayon} disabled={!FirebaseAPI.getUser().isDirector}>
                                        <MDBIcon icon="trash"/>
                                    </MDBBtn>
                                    <MDBBtn tag="a" floating color="info" className="mr-2 my-1" disabled={!FirebaseAPI.getUser().isDirector && FirebaseAPI.getUser().rayonId!==rayon.rayonId} onClick={e=>this.props.history.push(`/rayon/edit/${rayon.rayonId}`)}>
                                        <MDBIcon icon="pen"/>
                                    </MDBBtn>
                                </>
                            ) : ""}
                            <MDBIcon
                                icon={isOpen ? 'angle-down' : 'angle-up'}
                            />
                        </div>
                    </MDBCollapseHeader>
                    <MDBCollapse isOpen={isOpen}>
                        <MDBCardBody>
                            <MDBTable striped>
                                <MDBTableHead>
                                    <tr>
                                        <th>Article</th>
                                        <th>Prix HT (€)</th>
                                        <th>Taxe (%)</th>
                                        <th>Stock</th>
                                        <th>Action</th>
                                    </tr>
                                </MDBTableHead>
                                <MDBTableBody>
                                    {this.state.listArticleRef.map((articleDoc, index)=>(
                                        <ArticleLine articleDoc={articleDoc} key={index}/>
                                    ))}
                                </MDBTableBody>
                            </MDBTable>
                        </MDBCardBody>
                    </MDBCollapse>
                </MDBCard>
            );
        } else {
            return (
                <MDBRow center={true} className="mt-3">
                    <MDBCol size="6">
                        <MDBCard>
                            <MDBCardBody>
                                <img src={gifError} alt="gif-error" className="text-center img-fluid"/>
                                <h2>Erreur <span role="img" aria-label="error-smiley">&#128551;</span> : {`${this.state.errorCode}`}</h2>
                                <h4>=> {`${this.state.errorMessage}`}</h4>
                            </MDBCardBody>
                        </MDBCard>
                    </MDBCol>
                </MDBRow>
            )
        }
    }
}
export default withRouter(RayonView)