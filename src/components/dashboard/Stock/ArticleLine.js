import React, {Component} from 'react';
import {MDBBtn, MDBCard, MDBCardBody, MDBCol, MDBIcon, MDBRow, MDBSpinner} from "mdbreact";
import gifError from "../../../assets/img/error.gif";
import Article from "../../../model/Article";
import {withRouter} from "react-router";
import FirebaseAPI from "../../../api/FirebaseAPI";

class ArticleLine extends Component{
    state = {
        loading: true
    };

    componentDidMount() {
        let article;
        if(process.env.NODE === "test"){
            const {articleProp} = this.props;
            article = articleProp;
        } else {
            const {articleDoc} = this.props;
            article = Article.fromData(articleDoc.data());
        }

        this.setState({
            loading: false,
            status: true,
            article: article
        });
    }

    addQuantity = () => {
        let {article} = this.state;
        article.quantite = article.quantite + 1;
        FirebaseAPI.getDatabase()
            .collection("Article")
            .doc(article.articleId)
            .update(article.toData());
    };

    removeQuantity = () => {
        let {article} = this.state;
        article.quantite = article.quantite - 1;
        FirebaseAPI.getDatabase()
            .collection("Article")
            .doc(article.articleId)
            .update(article.toData());
    };


    render() {
        const {loading} = this.state;

        if(loading){
            return(
                <tr>
                    <td><MDBSpinner/></td>
                    <td><MDBSpinner/></td>
                    <td><MDBSpinner/></td>
                    <td><MDBSpinner/></td>
                    <td><MDBSpinner/></td>
                </tr>
            );
        } else if(this.state.status) {
            return (
                <tr className="line-1dot3-rem">
                    <td>{this.state.article.nom}</td>
                    <td>{this.state.article.prix} €</td>
                    <td>{this.state.article.tva} %</td>
                    <td>{this.state.article.quantite}</td>
                    <td>
                        <MDBBtn tag="a" size="lg" floating color="success" className="mr-2 my-1" onClick={this.addQuantity}>
                            <MDBIcon icon="plus"/>
                        </MDBBtn>
                        <MDBBtn tag="a" size="lg" floating color="danger" disabled={this.state.article.quantite<=0 || (!FirebaseAPI.getUser().isDirector && FirebaseAPI.getUser().rayonId!==this.state.article.rayonId)} className="mr-2 my-1" onClick={this.removeQuantity}>
                            <MDBIcon icon="minus"/>
                        </MDBBtn>
                        <MDBBtn tag="a" size="lg" floating color="info" className="mr-2 my-1" disabled={!FirebaseAPI.getUser().isDirector && FirebaseAPI.getUser().rayonId!==this.state.article.rayonId} onClick={e=>this.props.history.push(`/article/edit/${this.state.article.articleId}`)}>
                            <MDBIcon icon="pen"/>
                        </MDBBtn>
                    </td>
                </tr>
            );
        } else {
            return (
                <MDBRow center={true} className="mt-3">
                    <MDBCol size="6">
                        <MDBCard>
                            <MDBCardBody>
                                <img src={gifError} alt="gif-error" className="text-center img-fluid"/>
                                <h2>Erreur <span role="img" aria-label="error-smiley">&#128551;</span> : {`${this.state.errorCode}`}</h2>
                                <h4>=> {`${this.state.errorMessage}`}</h4>
                            </MDBCardBody>
                        </MDBCard>
                    </MDBCol>
                </MDBRow>
            )
        }
    }
}
export default withRouter(ArticleLine)