import React, {Component} from 'react';
import {MDBAnimation, MDBBtnFixed, MDBBtnFixedItem, MDBCard, MDBCardBody, MDBCol, MDBContainer, MDBRow} from 'mdbreact';
import FirebaseAPI from "../../../api/FirebaseAPI";
import gifLoading from "../../../assets/img/loading.gif";
import gifError from "../../../assets/img/error.gif";
import Rayon from "../../../model/Rayon";
import RayonView from "./RayonView";
import ToolPadSingleton from "../../../api/ToolPad";

class StockDashboard extends Component {
    state = {
        listRayon: [],
        loading: true,
        buttonStyle: {
            transform: "scaleY(0.4) scaleX(0.4) translateY(10px) translateX(0)",
            opacity: "0"
        }
    };

    onHover = () => {
        this.setState({
            buttonStyle: {
                transform: "scaleY(1) scaleX(1) translateY(0) translateX(0)",
                opacity: "1"
            }
        });
    };

    goToUser = () => {
        const {history} = this.props;
        history.push("/users");
    };

    goTo = (url) => event => {
        const {history} = this.props;
        history.push(url);
    };

    onMouseLeave = () => {
        this.setState({
            buttonStyle: {
                transform: "scaleY(0.4) scaleX(0.4) translateY(10px) translateX(0)",
                opacity: "0"
            }
        });
    };

    prepareRayon(doc) {
        const rayonData = doc.data();
        return Rayon.fromData(rayonData);
    }

    componentDidMount() {
        ToolPadSingleton.getInstance()
            .setButton({
                buttonId: 1,
                text: "User Dashboard",
                color: 12,
                icon: "person_Material",
                callback: this.goToUser
            });

        FirebaseAPI.getDatabase().collection("Rayon")
            .onSnapshot((docSnapshot)=>{
                let listRayon = [];
                listRayon = docSnapshot.docs.map(this.prepareRayon);
                this.setState({
                    loading: false,
                    status: true,
                    listRayon: listRayon
                });
            }, (error)=>{
                this.setState({
                    loading: false,
                    status: false,
                    errorMessage: error.getMessage(),
                    errorCode: error.getCode()
                });
            });
    }

    render() {
        const {loading, listRayon} = this.state;
        if(loading){
            return (
                <MDBContainer fluid>
                    <MDBRow center={true}>
                        <MDBCol size="2" className="mt-5">
                            <MDBCard>
                                <MDBCardBody>
                                    <img src={gifLoading} alt="gif-loading"/>
                                </MDBCardBody>
                            </MDBCard>
                        </MDBCol>
                    </MDBRow>
                </MDBContainer>
            )
        } else if (this.state.status){
            return (
                <section>
                    <MDBAnimation type="fadeIn">
                        <MDBContainer fluid>
                            {listRayon.map((rayon, index)=>(
                                <RayonView rayon={rayon} key={index}/>
                            ))}
                            <RayonView rayon={{nom: "Non affecté", rayonId: null}}/>
                        </MDBContainer>
                    </MDBAnimation>
                    <MDBBtnFixed
                        onMouseEnter={this.onHover}
                        onMouseLeave={this.onMouseLeave}
                        floating
                        size="lg"
                        color="red"
                        icon="plus"
                        style={{ bottom: "10px", right: "24px" }}
                    >
                        <MDBBtnFixedItem
                            buttonStyle={this.state.buttonStyle}
                            color="light-blue"
                            icon="user"
                            onClick={this.goTo("/user/add")}
                        />
                        <MDBBtnFixedItem
                            buttonStyle={this.state.buttonStyle}
                            color="orange"
                            icon="store"
                            onClick={this.goTo("/rayon/add")}
                        />
                        <MDBBtnFixedItem
                            buttonStyle={this.state.buttonStyle}
                            color="purple"
                            icon="database"
                            onClick={this.goTo("/article/add")}
                        />
                    </MDBBtnFixed>
                </section>
            );
        } else {
            return (
                <MDBContainer fluid>
                    <MDBRow center={true} className="mt-3">
                        <MDBCol size="6">
                            <MDBCard>
                                <MDBCardBody>
                                    <img src={gifError} alt="gif-error" className="text-center img-fluid"/>
                                    <h2>Erreur <span role="img" aria-label="error-smiley">&#128551;</span> : {`${this.state.errorCode}`}</h2>
                                    <h4>=> {`${this.state.errorMessage}`}</h4>
                                </MDBCardBody>
                            </MDBCard>
                        </MDBCol>
                    </MDBRow>
                </MDBContainer>
            )
        }
    }
}

export default StockDashboard;
