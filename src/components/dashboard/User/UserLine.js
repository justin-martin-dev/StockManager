import React, {Component} from 'react';
import {MDBBtn, MDBCard, MDBCardBody, MDBCol, MDBIcon, MDBRow, MDBSpinner} from "mdbreact";
import gifError from "../../../assets/img/error.gif";
import User from "../../../model/User";
import {withRouter} from "react-router";
import * as Swal from "sweetalert2";
import FirebaseAPI from "../../../api/FirebaseAPI";

class UserLine extends Component{
    state = {
        loading: true
    };

    componentDidMount() {
        const {userDoc} = this.props;
        const user = User.fromData(userDoc.data());

        this.setState({
            loading: false,
            status: true,
            user: user
        });
    }

    deleteUser = event => {
        Swal.fire({
            title: 'Etes-vous sûr ?',
            text: `Vous ne pourrez revenir en arrière après la suppréssion de l'utilisateur : ${this.state.user.prenom} ${this.state.user.nom}`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui, le supprimer!'
        }).then((result) => {
            if (result.value) {
                FirebaseAPI.getDatabase().collection("User").doc(this.state.user.userId).delete()
                    .then(()=>{
                        Swal.fire(
                            'Supprimé!',
                            `L'utilisateur ${this.state.user.prenom} ${this.state.user.nom} a été supprimé`,
                            'success'
                        )
                    })
                    .catch((error)=>{
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: `Erreur : ${error.message}`,
                        })
                    })
            }
        })
    };

    render() {
        const {loading} = this.state;

        if(loading){
            return(
                <tr>
                    <td><MDBSpinner/></td>
                    <td><MDBSpinner/></td>
                    <td><MDBSpinner/></td>
                    <td><MDBSpinner/></td>
                </tr>
            );
        } else if(this.state.status) {
            return (
                <tr className="line-1dot3-rem">
                    <td>{this.state.user.prenom}</td>
                    <td>{this.state.user.nom}</td>
                    <td><a className="link" href={`mailto:${this.state.user.email}`}>{this.state.user.email}</a></td>
                    <td>
                        <MDBBtn tag="a" size="lg" floating color="info" className="mr-2 my-1" onClick={e=>this.props.history.push(`/user/edit/${this.state.user.userId}`)} disabled={!FirebaseAPI.getUser().isDirector && FirebaseAPI.getUser().userId!==this.state.user.userId}>
                            <MDBIcon icon="pen"/>
                        </MDBBtn>
                        <MDBBtn tag="a" size="lg" floating color="danger" className="mr-2 my-1" onClick={this.deleteUser} disabled={!FirebaseAPI.getUser().isDirector && FirebaseAPI.getUser().userId!==this.state.user.userId}>
                            <MDBIcon icon="trash"/>
                        </MDBBtn>

                    </td>
                </tr>
            );
        } else {
            return (
                <MDBRow center={true} className="mt-3">
                    <MDBCol size="6">
                        <MDBCard>
                            <MDBCardBody>
                                <img src={gifError} alt="gif-error" className="text-center img-fluid"/>
                                <h2>Erreur <span role="img" aria-label="error-smiley">&#128551;</span> : {`${this.state.errorCode}`}</h2>
                                <h4>=> {`${this.state.errorMessage}`}</h4>
                            </MDBCardBody>
                        </MDBCard>
                    </MDBCol>
                </MDBRow>
            )
        }
    }
}
export default withRouter(UserLine)