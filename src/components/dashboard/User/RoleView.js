import React, {Component} from 'react';
import {
    MDBBadge,
    MDBCard,
    MDBCardBody, MDBCol,
    MDBCollapse,
    MDBCollapseHeader,
    MDBIcon, MDBRow, MDBSpinner,
    MDBTable,
    MDBTableBody,
    MDBTableHead
} from "mdbreact";
import FirebaseAPI from "../../../api/FirebaseAPI";
import gifError from "../../../assets/img/error.gif";
import UserLine from "./UserLine";

class RoleView extends Component {
    state = {
        isOpen : true,
        loading: true
    };

    toggleCollapse = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    };


    componentDidMount() {
        FirebaseAPI.getDatabase().collection("User")
            .where("rayonId", "==", this.props.rayon.rayonId)
            .onSnapshot((docSnapshot)=>{
                this.setState({
                    loading: false,
                    status: true,
                    isOpen: docSnapshot.docs.length > 0,
                    listUserRef: docSnapshot.docs.filter(doc => doc.data().isDirector === this.props.isDirectorLine)
                });
            }, (error)=>{
                this.setState({
                    loading: false,
                    status: false,
                    errorMessage: error.message,
                    errorCode: error.code
                });
            })
    }

    render() {
        const {isOpen, loading} = this.state;

        if(loading){
            return <MDBSpinner/>
        } else if(this.state.status) {
            return (
                <MDBCard className='mt-3'>
                    <MDBCollapseHeader
                        tagClassName='d-flex justify-content-between white-text'
                        onClick={this.toggleCollapse}
                    >
                        <div>
                            {this.props.isDirectorLine ? "Directeur" : (this.props.rayon.rayonId != null ? this.props.rayon.nom : "Non affecté")}
                            <MDBBadge color="secondary" className="ml-2">{this.state.listUserRef.length}</MDBBadge>
                        </div>
                        <MDBIcon
                            icon={isOpen ? 'angle-down' : 'angle-up'}
                        />
                    </MDBCollapseHeader>
                    <MDBCollapse isOpen={isOpen}>
                        <MDBCardBody>
                            <MDBTable striped>
                                <MDBTableHead>
                                    <tr>
                                        <th>Prénom</th>
                                        <th>Nom</th>
                                        <th>Email</th>
                                        <th>Action</th>
                                    </tr>
                                </MDBTableHead>
                                <MDBTableBody>
                                    {this.state.listUserRef.map((userDoc, index)=>(
                                        <UserLine userDoc={userDoc} key={index}/>
                                    ))}
                                </MDBTableBody>
                            </MDBTable>
                        </MDBCardBody>
                    </MDBCollapse>
                </MDBCard>
            );
        } else {
            return (
                <MDBRow center={true} className="mt-3">
                    <MDBCol size="6">
                        <MDBCard>
                            <MDBCardBody>
                                <img src={gifError} alt="gif-error" className="text-center img-fluid"/>
                                <h2>Erreur <span role="img" aria-label="error-smiley">&#128551;</span> : {`${this.state.errorCode}`}</h2>
                                <h4>=> {`${this.state.errorMessage}`}</h4>
                            </MDBCardBody>
                        </MDBCard>
                    </MDBCol>
                </MDBRow>
            )
        }
    }
}
export default RoleView