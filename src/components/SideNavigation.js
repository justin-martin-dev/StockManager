import React from 'react';
import {
  MDBSideNavLink,
  MDBSideNavNav,
  MDBSideNav,
  MDBIcon
} from 'mdbreact';

import "../assets/css/App.css"

class SideNavigation extends React.Component {
  render() {
    return (
        <div className='white-skin'>
            <MDBSideNav
              bg='https://mdbootstrap.com/img/Photos/Others/sidenav4.jpg'
              mask='strong'
              fixed
              breakWidth={this.props.breakWidth}
              triggerOpening={this.props.triggerOpening}
              style={{ transition: 'padding-left .3s' }}
            >
                <MDBSideNavNav>
                    <MDBSideNavLink to="/stock" topLevel>
                        <MDBIcon icon="database" className="mr-2" />Stock
                    </MDBSideNavLink>
                    <MDBSideNavLink to="/users" topLevel>
                        <MDBIcon icon="users" className="mr-2" />Utilisateurs
                    </MDBSideNavLink>
                </MDBSideNavNav>
            </MDBSideNav>
        </div>
    );
  }
}

export default SideNavigation;
