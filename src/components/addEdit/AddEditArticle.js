import FirebaseAPI from "../../api/FirebaseAPI";
import Rayon from "../../model/Rayon";
import Swal from "sweetalert2";
import {
    MDBAnimation,
    MDBBtn,
    MDBCard,
    MDBCardBody,
    MDBCol,
    MDBContainer,
    MDBInput,
    MDBRow,
    MDBSelect, MDBSelectInput, MDBSelectOption, MDBSelectOptions, MDBSpinner,
    MDBView
} from "mdbreact";
import React from "react";
import * as uuid from "uuid";

class AddEditArticle extends React.Component{
    constructor(props){
        super(props);
        let {articleId} = this.props.match.params;
        this.isEdit = articleId !== undefined;
        this.state={
            listRayon: [],
            loading: true,
            actionPending: false,
            article: {
                nom: "",
                rayonId: "",
                prix: undefined,
                tva: undefined,
                quantite: undefined
            }
        };
    }


    formIsUnvalid = () => {
        return this.state.article.nom === "" ||
            this.state.article.prix === undefined ||
            this.state.article.tva === undefined ||
            this.state.article.quantite === undefined
    };

    componentDidMount() {
        if(!this.isEdit && !FirebaseAPI.getUser().isDirector && FirebaseAPI.getUser().rayonId===null) //mode creation
            this.props.history.push('/unallowed-page/director-chefRayon');

        FirebaseAPI.getDatabase()
            .collection("Rayon")
            .onSnapshot((docSnapshot)=>{
                let listRayon = [];
                docSnapshot.docs.forEach((doc)=>{
                    const data = doc.data();
                    if(FirebaseAPI.getUser().isDirector || data.rayonId === FirebaseAPI.getUser().rayonId){
                        const rayon = Rayon.fromData({
                            nom: data.nom,
                            rayonId: data.rayonId
                        });
                        listRayon.push(rayon);
                    }
                });

                if(this.isEdit){
                    FirebaseAPI.getDatabase()
                        .collection("Article")
                        .doc(this.props.match.params.articleId)
                        .get()
                        .then((doc)=>{
                            const data = doc.data();
                            if(!FirebaseAPI.getUser().isDirector && FirebaseAPI.getUser().rayonId!==data.rayonId)
                                this.props.history.push('/unallowed-page/director-chefRayon');

                            this.setState({
                                article: data,
                                loading: false,
                                listRayon: listRayon
                            });
                        })
                } else {
                    this.setState({
                        listRayon: listRayon,
                        loading: false
                    });
                }
            })
    }

    changeProperty = (nameProperty, value) => {
        let {article} = this.state;
        article[nameProperty] = value;
        this.setState({
            article : article
        });
    };

    sendForm = () => {
        this.setState({
            actionPending: true
        });
        if(!this.isEdit){
            let {article} = this.props;
            article["articleId"] = uuid.v1();

            FirebaseAPI.getDatabase()
                .collection("Article")
                .doc(article.articleId)
                .set(article)
                .then(()=>{
                    this.setState({
                        actionPending: false
                    });
                    Swal.fire({
                        icon: 'success',
                        title: 'Article créé',
                        text: 'L\'article a bien été créé!',
                    })
                })
                .catch((error)=>{
                    this.setState({
                        actionPending: false
                    });
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: `Erreur : ${error.message}`,
                    })
                });
        } else {
            FirebaseAPI.getDatabase()
                .collection("Article")
                .doc(this.state.article.articleId)
                .update(this.state.article)
                .then(()=>{
                    this.setState({
                        actionPending: false
                    });
                    Swal.fire({
                        icon: 'success',
                        title: 'Article modifié',
                        text: 'L\'article a bien été modifié!',
                    })
                })
                .catch((error)=>{
                    this.setState({
                        actionPending: false
                    });
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: `Erreur : ${error.message}`,
                    })
                });
        }
    };

    render(){
        const {loading} = this.state;
        return(
            <MDBAnimation type="fadeIn">
                {loading ?
                    (<MDBSpinner/>) :
                    (<div id='profile-v2' className='mb-5'>
                        <MDBContainer fluid>
                            <MDBRow center={true}>
                                <MDBCol lg='8' className='mb-r'>
                                    <MDBCard narrow>
                                        <MDBView cascade className='mdb-color lighten-3 card-header'>
                                            <h5 className='mb-0 font-weight-bold text-center text-white'>
                                                {this.isEdit ? "Editer un article" : "Ajouter un article"}
                                            </h5>
                                        </MDBView>
                                        <MDBCardBody className='text-center'>
                                            <MDBRow>
                                                <MDBCol md='6'>
                                                    <MDBInput
                                                        type='text'
                                                        label='Nom'
                                                        value={this.state.article.nom}
                                                        getValue={(value) => this.changeProperty("nom", value)}
                                                        required
                                                        className={this.state.article.nom !== undefined && this.state.article.nom !== "" ? "is-valid" : "is-invalid"}
                                                    >
                                                        <div className="valid-feedback">Nom valide!</div>
                                                        <div className="invalid-feedback">Veuillez remplir ce champ!
                                                        </div>
                                                    </MDBInput>
                                                </MDBCol>
                                                <MDBCol md='6'>
                                                    <MDBInput
                                                        type='number'
                                                        label='Prix'
                                                        value={this.state.article.prix}
                                                        getValue={(value) => this.changeProperty("prix", value)}
                                                        required
                                                        className={this.state.article.prix !== undefined ? "is-valid" : "is-invalid"}
                                                    >
                                                        <div className="valid-feedback">Prix valide!</div>
                                                        <div className="invalid-feedback">Veuillez remplir ce champ!
                                                        </div>
                                                    </MDBInput>
                                                </MDBCol>
                                            </MDBRow>
                                            <MDBRow>
                                                <MDBCol md='6'>
                                                    <MDBInput
                                                        type='number'
                                                        label='TVA (%)'
                                                        value={this.state.article.tva}
                                                        getValue={(value) => this.changeProperty("tva", value)}
                                                        required
                                                        className={this.state.article.tva !== undefined ? "is-valid" : "is-invalid"}
                                                    >
                                                        <div className="valid-feedback">TVA valide!</div>
                                                        <div className="invalid-feedback">Veuillez remplir ce champ!
                                                        </div>
                                                    </MDBInput>
                                                </MDBCol>
                                                <MDBCol md='6'>
                                                    <MDBInput
                                                        type='number'
                                                        label='Quantite'
                                                        value={this.state.article.quantite}
                                                        getValue={(value) => this.changeProperty("quantite", value)}
                                                        required
                                                        className={this.state.article.quantite !== undefined ? "is-valid" : "is-invalid"}
                                                    >
                                                        <div className="valid-feedback">Quantite valide!</div>
                                                        <div className="invalid-feedback">Veuillez remplir ce champ!
                                                        </div>
                                                    </MDBInput>
                                                </MDBCol>
                                            </MDBRow>
                                            <MDBRow>
                                                <MDBCol md='6'>
                                                    <MDBSelect
                                                        label="Rayon"
                                                        getValue={(value) => {
                                                            this.changeProperty("rayonId", (value[0] === "null" ? null : value[0]))
                                                        }}
                                                    >
                                                        <MDBSelectInput selected="Selectionner le rayon"/>
                                                        <MDBSelectOptions>
                                                            <MDBSelectOption value={"null"} checked={this.state.article.rayonId === null}>Non affecté</MDBSelectOption>
                                                            {this.state.listRayon.map((rayon, index)=>(
                                                                <MDBSelectOption key={index} value={rayon.rayonId} checked={this.state.article.rayonId === rayon.rayonId}>{rayon.nom}</MDBSelectOption>
                                                            ))}
                                                        </MDBSelectOptions>
                                                    </MDBSelect>
                                                </MDBCol>
                                            </MDBRow>
                                            <MDBRow className="mt-5">
                                                <MDBCol md='12' className='text-center'>
                                                    {this.state.actionPending ? (
                                                        <MDBBtn color='info' type='submit' rounded disabled={true}>
                                                            En cours...
                                                            <div className="spinner-border text-white" role="status">
                                                                <span className="sr-only">Loading...</span>
                                                            </div>
                                                        </MDBBtn>
                                                    ) : (
                                                        <MDBBtn color='info' type='submit' rounded disabled={this.formIsUnvalid()} onClick={this.sendForm}>
                                                            {this.isEdit ? "Modifier l'article" : "Ajouter l'article"}
                                                        </MDBBtn>
                                                    )}
                                                </MDBCol>
                                            </MDBRow>
                                        </MDBCardBody>
                                    </MDBCard>
                                </MDBCol>
                            </MDBRow>
                        </MDBContainer>
                    </div>)
                }
            </MDBAnimation>
        )
    }
}

export default AddEditArticle