import React, {Component} from 'react'
import {
    MDBRow,
    MDBCol,
    MDBCard,
    MDBView,
    MDBCardBody,
    MDBInput,
    MDBContainer,
    MDBBtn,
    MDBAnimation, MDBSelect, MDBSpinner, MDBSelectInput, MDBSelectOptions, MDBSelectOption
} from 'mdbreact';

import Swal from 'sweetalert2'

import Format from "../../utils/Format";
import FirebaseAPI from "../../api/FirebaseAPI";
import Rayon from "../../model/Rayon";
import User from "../../model/User";
import {withRouter} from "react-router";
import ToolPadSingleton from "../../api/ToolPad";

class AddEditUser extends Component{
    constructor(props){
        super(props);
        let {userId} = this.props.match.params;
        this.isEdit = userId !== undefined;
        this.state = {
            loading: true,
            actionPending: false,
            listRayon: [],
            listUserEmail: [],
            user: {
                email: "",
                password: "",
                prenom: "",
                nom: "",
                rayon: "",
                role: ""
            }
        };
    }

    listRole = [
        {
            text: "Chef de magasin",
            value: "chef_magasin"
        }, {
            text: "Chef de rayon",
            value: "chef_rayon"
        }
    ];

    formIsUnvalid = () => {
        return this.state.user.email === "" ||
            this.state.user.prenom === "" ||
            this.state.user.nom === "" ||
            this.state.user.role === "" ||
            (this.state.user.role === "chef_rayon" && this.state.user.rayon === "");
    };

    prepareRayon(docRayon){
        const data = docRayon.data();
        return Rayon.fromData(data);
    }

    componentDidMount() {
        if(!this.isEdit && !FirebaseAPI.getUser().isDirector) //mode creation
            this.props.history.push('/unallowed-page/director');

        if(this.isEdit && !FirebaseAPI.getUser().isDirector && FirebaseAPI.getUser().userId!==this.props.match.params.userId)
            this.props.history.push('/unallowed-page/director-userSelf');


        FirebaseAPI.getDatabase()
            .collection("Rayon")
            .get()
            .then(async (docSnapshot)=>{
                let listRayon = docSnapshot.docs.map(this.prepareRayon);

                let userData = {
                    email: "",
                    password: "",
                    prenom: "",
                    nom: "",
                    rayon: null,
                    role: ""
                };
                let listUserEmail = [];
                if(this.isEdit) {
                    let userDoc = await FirebaseAPI.getDatabase().collection("User")
                        .doc(this.props.match.params.userId)
                        .get();
                    if(userDoc.exists) {
                        userData = userDoc.data();
                        userData["password"] = "";
                        userData["role"] = userData.isDirector ? "chef_magasin" : "chef_rayon";
                    }
                } else {
                    let usersDoc = await FirebaseAPI.getDatabase().collection("User")
                        .get();
                    listUserEmail = usersDoc.docs.map(doc=>{return doc.data().email});
                }

                this.setState({
                    listRayon: listRayon,
                    listUserEmail: listUserEmail,
                    user: userData,
                    loading: false
                });
            });
    }

    changeProperty = (nameProperty, value) => {
        if(this.formIsUnvalid()){

        }
        let {user} = this.state;
        user[nameProperty] = value;
        this.setState({
            user : user
        });
    };

    sendForm = () => {
        let {password, email, nom, prenom, rayon, role} = this.state.user;
        this.setState({
            actionPending: true
        });

        if(!this.isEdit) {
            FirebaseAPI.getAuth().createUserWithEmailAndPassword(email, password)
                .then((userCreds) => {
                    const newUser = User.fromData({
                        userId: userCreds.user.uid,
                        nom: nom,
                        prenom: prenom,
                        email: email,
                        isDirector: role === "chef_magasin",
                        rayonId: role === "chef_magasin" ? null : rayon
                    });
                    FirebaseAPI.getDatabase()
                        .collection("User")
                        .doc(newUser.userId)
                        .set(newUser.toData())
                        .then(() => {
                            Swal.fire({
                                icon: 'success',
                                title: 'Utilisateur créé',
                                text: 'Le compte utilisateur à bien été créé!',
                            })
                        })
                        .catch((error) => {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: `Erreur : ${error.message}`,
                            })
                        });
                })
                .catch((error) => {
                    console.error("erreur", error);
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: `Erreur : ${error.message}`,
                    })
                });
        } else {
            if(rayon === undefined)
                rayon = null;

            FirebaseAPI.getDatabase().collection("User").doc(this.state.user.userId).update({
                nom: nom,
                prenom: prenom,
                email: email,
                rayonId: role === "chef_magasin" ? null : rayon,
                isDirector: role === "chef_magasin"
            }).then(()=>{
                if(password !== ""){
                    const updatePasswordUser = FirebaseAPI.getFunctions().httpsCallable("updatePasswordUser");
                    updatePasswordUser({changeValue: {password: this.state.user.password, userId: this.state.user.userId}})
                        .then(() => {
                            Swal.fire({
                                icon: 'success',
                                title: 'Utilisateur modifié',
                                text: 'Le compte utilisateur a bien été modifié!',
                            });
                            this.setState({
                                actionPending: false
                            });
                        })
                        .catch((error) => {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: `Erreur : ${error.message}`,
                            });
                            this.setState({
                                actionPending: false
                            });
                        });
                }
                else {
                    Swal.fire({
                        icon: 'success',
                        title: 'Utilisateur modifié',
                        text: 'Le compte utilisateur a bien été modifié!',
                    });
                    this.setState({
                        actionPending: false
                    });
                }
            });
        }

    };

    render(){
        const {loading} = this.state;
        return(
            <MDBAnimation type="fadeIn">
                {loading ?
                    (<MDBSpinner/>) :
                    (<div id='profile-v2' className='mb-5'>
                        <MDBContainer fluid>
                            <MDBRow center={true}>
                                <MDBCol lg='8' className='mb-r'>
                                    <MDBCard narrow>
                                        <MDBView cascade className='mdb-color lighten-3 card-header'>
                                            <h5 className='mb-0 font-weight-bold text-center text-white'>
                                                {this.isEdit ? "Editer un utilisateur" : "Ajouter un utilisateur"}
                                            </h5>
                                        </MDBView>
                                        <MDBCardBody className='text-center'>
                                            <MDBRow>
                                                <MDBCol md='6'>
                                                    <MDBInput
                                                        type='text'
                                                        label='Prenom'
                                                        value={this.state.user.prenom}
                                                        getValue={(value)=>this.changeProperty("prenom", value)}
                                                        required
                                                        className={this.state.user.prenom !== undefined && this.state.user.prenom !== "" ? "is-valid" : "is-invalid"}
                                                    >
                                                        <div className="valid-feedback">Prenom valide!</div>
                                                        <div className="invalid-feedback">Veuillez remplir ce champ!</div>
                                                    </MDBInput>
                                                </MDBCol>
                                                <MDBCol md='6'>
                                                    <MDBInput
                                                        type='text'
                                                        label='Nom'
                                                        value={this.state.user.nom}
                                                        getValue={(value)=>this.changeProperty("nom", value)}
                                                        required
                                                        className={this.state.user.nom !== undefined && this.state.user.nom !== "" ? "is-valid" : "is-invalid"}
                                                    >
                                                        <div className="valid-feedback">Nom valide!</div>
                                                        <div className="invalid-feedback">Veuillez remplir ce champ!</div>
                                                    </MDBInput>
                                                </MDBCol>
                                            </MDBRow>
                                            <MDBRow>
                                                <MDBCol md='6'>
                                                    <MDBInput
                                                        type='text'
                                                        label='Email'
                                                        value={this.state.user.email}
                                                        getValue={(value)=>this.changeProperty("email", value)}
                                                        required
                                                        className={this.state.user.email !== undefined && Format.isEmail(this.state.user.email) && (!this.isEdit ? !this.state.listUserEmail.includes(this.state.user.email) : true) ? "is-valid" : "is-invalid"}
                                                    >
                                                        <div className="valid-feedback">Email valide!</div>
                                                        <div className="invalid-feedback">Veuillez remplir ce champ avec un email valide!</div>
                                                    </MDBInput>
                                                </MDBCol>
                                                <MDBCol md='6'>
                                                    <MDBInput
                                                        type='password'
                                                        label='Password'
                                                        value={this.state.user.password}
                                                        getValue={(value)=>this.changeProperty("password", value)}
                                                        required
                                                        className={(this.state.user.password !== "" && this.state.user.password.length > 5) || (this.state.user.password === "" && this.isEdit) ? "is-valid" : "is-invalid"}
                                                    >
                                                        {this.isEdit ? (
                                                            <React.Fragment>
                                                                <div className="valid-feedback">Laisser vide pour ne pas changer le mot de passe, Mot de passe valide!</div>
                                                                <div className="invalid-feedback">Veuillez remplir ce champ avec un mot de passe de plus de 5 caractères!</div>
                                                            </React.Fragment>
                                                        ) : (
                                                            <React.Fragment>
                                                                <div className="valid-feedback">Mot de passe valide!</div>
                                                                <div className="invalid-feedback">Veuillez remplir ce champ avec un mot de passe de plus de 5 caractères!</div>
                                                            </React.Fragment>
                                                        )}
                                                    </MDBInput>
                                                </MDBCol>
                                            </MDBRow>
                                            {FirebaseAPI.getUser().isDirector ? (
                                                <MDBRow>
                                                    <MDBCol md='6'>
                                                        <MDBSelect
                                                            label="Rôle"
                                                            getValue={(value)=>{this.changeProperty("role", value[0])}}
                                                        >
                                                            <MDBSelectInput selected="Selectionner le rôle"/>
                                                            <MDBSelectOptions>
                                                                {this.listRole.map((role, index)=>(
                                                                    <MDBSelectOption key={index} value={role.value} checked={this.state.user.role === role.value}>{role.text}</MDBSelectOption>
                                                                ))}
                                                            </MDBSelectOptions>
                                                        </MDBSelect>
                                                    </MDBCol>
                                                    {this.state.user.role === "chef_rayon" ? (
                                                        <MDBCol md='6'>
                                                            <MDBSelect
                                                                label="Rayon"
                                                                getValue={(value)=>{this.changeProperty("rayon", (value[0] === "null" ? null : value[0]))}}
                                                            >
                                                                <MDBSelectInput selected="Selectionner le rayon"/>
                                                                <MDBSelectOptions>
                                                                    <MDBSelectOption value={"null"} checked={this.state.user.rayonId === null}>Non affecté</MDBSelectOption>
                                                                    {this.state.listRayon.map((rayon, index)=>(
                                                                        <MDBSelectOption key={index} value={rayon.rayonId} checked={this.state.user.rayonId === rayon.rayonId}>{rayon.nom}</MDBSelectOption>
                                                                    ))}
                                                                </MDBSelectOptions>
                                                            </MDBSelect>
                                                        </MDBCol>
                                                    ) : "" }
                                                </MDBRow>
                                            ) : ""}
                                            <MDBRow className="mt-5">
                                                <MDBCol md='12' className='text-center'>
                                                    {this.state.actionPending ? (
                                                        <MDBBtn color='info' type='submit' rounded disabled={true}>
                                                            En cours...
                                                            <div className="spinner-border text-white" role="status">
                                                                <span className="sr-only">Loading...</span>
                                                            </div>
                                                        </MDBBtn>
                                                    ) : (
                                                        <MDBBtn color='info' type='submit' rounded disabled={this.formIsUnvalid()} onClick={this.sendForm}>
                                                            {this.isEdit ? "Modifier l'utilisateur" : "Ajouter l'utilisateur"}
                                                        </MDBBtn>
                                                    )}
                                                </MDBCol>
                                            </MDBRow>
                                        </MDBCardBody>
                                    </MDBCard>
                                </MDBCol>
                            </MDBRow>
                        </MDBContainer>
                    </div>)
                }
            </MDBAnimation>
        )
    }
}

export default withRouter(AddEditUser);