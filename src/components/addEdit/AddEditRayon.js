import FirebaseAPI from "../../api/FirebaseAPI";
import Swal from "sweetalert2";
import {
    MDBAnimation,
    MDBBtn,
    MDBCard,
    MDBCardBody,
    MDBCol,
    MDBContainer,
    MDBInput,
    MDBRow, MDBSpinner,
    MDBView
} from "mdbreact";
import React from "react";
import * as uuid from "uuid";

class AddEditRayon extends React.Component{
    constructor(props) {
        super(props);
        let {rayonId} = this.props.match.params;
        this.isEdit = rayonId !== undefined;
        this.state = {
            loading: true,
            actionPending: false,
            rayon: {
                rayonId: "",
                nom: ""
            }
        };
    }

    componentDidMount() {
        if(!this.isEdit && !FirebaseAPI.getUser().isDirector) //mode creation
            this.props.history.push('/unallowed-page/director');

        let rayonData = {
            rayonId: "",
            nom: ""
        };

        if(this.isEdit){
            if(!FirebaseAPI.getUser().isDirector && FirebaseAPI.getUser().rayonId !== this.props.match.params.rayonId)
                this.props.history.push('/unallowed-page/director-chefRayon');

            FirebaseAPI.getDatabase()
                .collection("Rayon")
                .where("rayonId", "==", this.props.match.params.rayonId)
                .get()
                .then((rayonDoc)=>{
                    rayonData = rayonDoc.docs[0].data();
                    this.setState({
                        rayon : rayonData,
                        loading: false
                    })
                })
        }
        else {
            this.setState({
                rayon : rayonData,
                loading: false
            })
        }
    }

    formIsUnvalid = () => {
        return this.state.rayon.nom === ""
    };

    changeProperty = (nameProperty, value) => {
        let {rayon} = this.state;
        rayon[nameProperty] = value;
        this.setState({
            rayon : rayon
        });
    };

    sendForm = () => {
        if(this.isEdit){
            FirebaseAPI.getDatabase()
                .collection("Rayon")
                .doc(this.state.rayon.rayonId)
                .update(this.state.rayon)
                .then(()=>{
                    Swal.fire({
                        icon: 'success',
                        title: 'Rayon modifié',
                        text: 'Le rayon a bien été modifié!',
                    })
                })
                .catch((error)=>{
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: `Erreur : ${error.message}`,
                    })
                });
        } else {
            let rayonData = {
                rayonId: uuid.v1(),
                nom: this.state.rayon.nom
            };
            FirebaseAPI.getDatabase()
                .collection("Rayon")
                .doc(rayonData.rayonId)
                .set(rayonData)
                .then(()=>{
                    Swal.fire({
                        icon: 'success',
                        title: 'Rayon créé',
                        text: 'Le rayon a bien été créé!',
                    })
                })
                .catch((error)=>{
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: `Erreur : ${error.message}`,
                    })
                });
        }

    };

    render(){
        const {loading} = this.state;
        return(
            <MDBAnimation type="fadeIn">
                {loading ?
                    (<MDBSpinner/>) :
                    (<div id='profile-v2' className='mb-5'>
                        <MDBContainer fluid>
                            <MDBRow center={true}>
                                <MDBCol lg='8' className='mb-r'>
                                    <MDBCard narrow>
                                        <MDBView cascade className='mdb-color lighten-3 card-header'>
                                            <h5 className='mb-0 font-weight-bold text-center text-white'>
                                                Ajouter un rayon
                                            </h5>
                                        </MDBView>
                                        <MDBCardBody className='text-center'>
                                            <MDBRow>
                                                <MDBCol md='12'>
                                                    <MDBInput
                                                        type='text'
                                                        label='Nom'
                                                        value={this.state.rayon.nom}
                                                        getValue={(value)=>this.changeProperty("nom", value)}
                                                        required
                                                        className={this.state.rayon.nom !== undefined && this.state.rayon.nom !== "" ? "is-valid" : "is-invalid"}
                                                    >
                                                        <div className="valid-feedback">Nom valide!</div>
                                                        <div className="invalid-feedback">Veuillez remplir ce champ!</div>
                                                    </MDBInput>
                                                </MDBCol>
                                            </MDBRow>
                                            <MDBRow className="mt-5">
                                                <MDBCol md='12' className='text-center'>
                                                    {this.state.actionPending ? (
                                                        <MDBBtn color='info' type='submit' rounded disabled={true}>
                                                            En cours...
                                                            <div className="spinner-border text-white" role="status">
                                                                <span className="sr-only">Loading...</span>
                                                            </div>
                                                        </MDBBtn>
                                                    ) : (
                                                        <MDBBtn color='info' type='submit' rounded disabled={this.formIsUnvalid()} onClick={this.sendForm}>
                                                            {this.isEdit ? "Modifier le rayon" : "Ajouter le rayon"}
                                                        </MDBBtn>
                                                    )}
                                                </MDBCol>
                                            </MDBRow>
                                        </MDBCardBody>
                                    </MDBCard>
                                </MDBCol>
                            </MDBRow>
                        </MDBContainer>
                    </div>)
                }
            </MDBAnimation>
        )
    }
}

export default AddEditRayon;