import React, { Component } from 'react';
import '../assets/css/App.css';
import SideNavigation from './SideNavigation';
import TopNavigation from './TopNavigation';
import Routes from './Routes';
import ToolPadSingleton from "../api/ToolPad";
import {withRouter} from "react-router";

class RoutesWithNavigation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: false,
      windowWidth: 0,
      currentPage: '',
      sideNavToggled: false,
      breakWidth: 1400
    };
  }

  componentDidUpdate(prevProps, nextProps, snapshot) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      this.assessLocation(this.props.location.pathname); //modification du nom de page
    }
  }

  goToAddRayon = () =>{
    this.props.history.push("/rayon/add");
  };

  goToAddArticle = () =>{
    this.props.history.push("/article/add");
  };

  goToAddUser = () =>{
    this.props.history.push("/user/add");
  };

  UNSAFE_componentWillMount() {
    ToolPadSingleton.getInstance()
        .setInterface({
          interfaceType: "5Button_Icon",
          title: "Navigation"
        });
  }

  componentDidMount() {
    ToolPadSingleton.getInstance()
        .setButton({
          buttonId: 2,
          text: "Add User",
          color: 9,
          icon: "person_add",
          callback: this.goToAddUser
        });
    ToolPadSingleton.getInstance()
        .setButton({
          buttonId: 3,
          text: "Add Rayon",
          color: 9,
          icon: "store_Material",
          callback: this.goToAddRayon
        });
    ToolPadSingleton.getInstance()
        .setButton({
          buttonId: 4,
          text: "Add Article",
          color: 9,
          icon: "local_offer",
          callback: this.goToAddArticle
        });

    this.handleResize();
    window.addEventListener('resize', this.handleResize);
    this.assessLocation(this.props.location.pathname);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  handleResize = () => { //mise à jour de la taille pour affichage responsive
    this.setState({
      windowWidth: window.innerWidth
    });
  };

  toggleSideNav = () => { //Activation/Désactivation du mode sideNav
    if (this.state.windowWidth < this.state.breakWidth) {
      this.setState({
        sideNavToggled: !this.state.sideNavToggled
      });
    }
  };

  /** Relation nom de page => url **/
  assessLocation = location => {
    let locationString;
    switch (location) {
      // Dashboards:
      case '/stock':
        locationString = 'Gestion Stock';
        break;

      // Pages
      case '/login':
        locationString = 'Login page';
        break;

      // Profiles
      case '/users':
        locationString = 'Gestion Utilisateur';
        break;
      default:
    }
    this.setState({
      currentPage: locationString
    });
  };

  render() {
    const dynamicLeftPadding = {
      paddingLeft:
        this.state.windowWidth > this.state.breakWidth ? '240px' : '0'
    };

    return (
      <div className='app'>
        <div className='white-skin'>
          <SideNavigation
            breakWidth={this.state.breakWidth}
            style={{ transition: 'all .3s' }}
            triggerOpening={this.state.sideNavToggled}
            onLinkClick={() => this.toggleSideNav()}
          />
        </div>
        <div className='flexible-content white-skin'>
          <TopNavigation
            toggle={this.state.windowWidth < this.state.breakWidth}
            onSideNavToggleClick={this.toggleSideNav}
            routeName={this.state.currentPage}
            className='white-skin'
          />
          <main style={{ ...dynamicLeftPadding, margin: '6rem 2% 6rem' }}>
            <Routes onChange={() => this.assessLocation()} />
          </main>
        </div>
      </div>
    );
  }
}

export default withRouter(RoutesWithNavigation);
