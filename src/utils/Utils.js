class Utils {
    static formatListRender(list, nbCol){
        let newList = [];
        let subList = [];
        let nb = 0;
        list.forEach((value)=>{
            nb+=1;
            subList.push(value);
            if(nb === nbCol) {
                newList.push(subList);
                subList = [];
                nb = 0;
            }
        });
        return newList;
    }

    static formatListSelect(listPromo) {
        let listSelect = [];
        listPromo.forEach((promo)=>{
           listSelect.push({text: promo.nom, value: promo.promoId});
        });
        return listSelect;
    }
}

export default Utils;