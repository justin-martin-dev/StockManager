import FirebaseAdminAPI from "./lib/FirebaseAdminAPI";
import {Builder} from "selenium-webdriver";
import {querySelectorId, querySelectorXPath} from './lib/helper'
FirebaseAdminAPI.init();

require('selenium-webdriver/chrome');

const rootURL = process.env.URL_TEST;
var driver;

jest.setTimeout(1000 * 60 * 5);

console.log("url", rootURL);

beforeAll(async () => {
    driver = await new Builder().forBrowser('chrome').build();
    driver.manage().window().maximize();
});
it('initialises the context', async () => {
    await driver.get(rootURL)
});
afterAll(()=>{
    return driver.quit();
});