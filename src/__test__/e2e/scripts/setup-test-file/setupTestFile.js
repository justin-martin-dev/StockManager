'use strict';

(async function main() {
    const mergeFiles = require('merge-files');
    const path = require('path');
    const config = require('../../conf/e2e.config');

    const outputPath = path.join(__dirname, config.testMainFile);
    let inputPathList = [];

    config.testFiles.forEach((file)=>{
        inputPathList.push(path.join(__dirname, file));
    });

    const status = await mergeFiles(inputPathList, outputPath);

    if(status){
        console.log(`Success merging \n on : ${outputPath} \n with : ${inputPathList}`);
        process.exit(0);
    } else {
        console.log("Error during merging");
        process.exit(1);
    }
})();