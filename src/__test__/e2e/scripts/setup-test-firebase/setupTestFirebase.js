var argv = require('minimist')(process.argv.slice(2));

let FirebaseAdminAPI = require('../../lib/FirebaseAdminAPI');
const firebaseContent = require('../../conf/firebase.content-test.json');

FirebaseAdminAPI.init();
if(argv.hasOwnProperty("init") && argv.init){
    initFirebaseAuth()
        .then(()=>initFirebaseDatabase()
            .then(()=>{
                console.log("Success init");
                process.exit(0)
            })
            .catch(()=>{
                console.log("Error during init firebase database");
                process.exit(1)
            })
        ).catch(()=>{
            console.log("Error during init firebase auth");
            process.exit(1)
        });
} else if (argv.hasOwnProperty("clean") && argv.clean){
    cleanFirebaseAuth()
        .then(()=>cleanFirebaseDatabase()
            .then(()=>{
                console.log("Success clean");
                process.exit(0);
            })
            .catch(()=>{
                console.log("Error during clean firebase database");
                process.exit(1)
            })
        ).catch(()=>{
            console.log("Error during clean firebase auth");
            process.exit(1)
        });
} else {
    console.log(
        "Pls use with as it : \n" +
        "- node setupTestFirebase.js --init => to init firebase content (auth, database)\n"+
        "- node setupTestFirebase.js --clean => to clean firebase (auth, database)"
    );
}

async function cleanFirebaseAuth() {
    await Promise.all(firebaseContent.auth.map((user)=>{return FirebaseAdminAPI.getAuth().deleteUser(user.uid).catch((error)=>{console.log(error)})}));
}
async function cleanFirebaseDatabase() {
    for (const collection in firebaseContent.firestore) {
        await Promise.all(firebaseContent.firestore[collection].map((doc)=>{return FirebaseAdminAPI.getDatabase().collection(collection).doc(doc.docId).delete()}))
    }
}

/* Init function */

async function initFirebaseAuth() {
    await Promise.all(firebaseContent.auth.map((user)=>{return FirebaseAdminAPI.getAuth().createUser(user);}));
}
async function initFirebaseDatabase() {
    for (const collection in firebaseContent.firestore) {
        await Promise.all(firebaseContent.firestore[collection].map((doc)=>{return FirebaseAdminAPI.getDatabase().collection(collection).doc(doc.docId).set(doc.data)}))
    }
}