import FirebaseAdminAPI from "./lib/FirebaseAdminAPI";
import {Builder} from "selenium-webdriver";
import {querySelectorId, querySelectorXPath} from './lib/helper'
FirebaseAdminAPI.init();

require('selenium-webdriver/chrome');

const rootURL = "https://stockmanager-173da.web.app/";
var driver;

jest.setTimeout(1000 * 60 * 5);

console.log("url", rootURL);

beforeAll(async () => {
    driver = await new Builder().forBrowser('chrome').build();
    driver.manage().window().maximize();
});
it('initialises the context', async () => {
    await driver.get(rootURL)
});
afterAll(()=>{
    return driver.quit();
});/**** Login test ****/

describe('Verify empty field invalidity', () => {
    test('Verify empty field email', async () => {
        const inputField = await querySelectorId("input-email-field", driver);
        let className = await inputField.getAttribute("class");
        expect(className.includes("is-invalid")).toBe(true);
    });

    test('Verify empty field password', async () => {
        const inputField = await querySelectorId("input-password-field", driver);
        let className = await inputField.getAttribute("class");
        expect(className.includes("is-invalid")).toBe(true);
    });
});

describe('Verify well-formated valid field', () => {
    test('Verify well-formated email *****@company.fr', async () => {
        const inputField = await querySelectorId("input-email-field", driver);
        await inputField.sendKeys("alexandre.fauchard@company.fr");
        let className = await inputField.getAttribute("class");
        expect(className.includes("is-valid")).toBe(true);
    });

    test('Verify well-formated password nb>5', async () => {
        const inputField = await querySelectorId("input-password-field", driver);
        await inputField.sendKeys("lollollol");
        let className = await inputField.getAttribute("class");
        expect(className.includes("is-valid")).toBe(true);
    });
});

describe('Login function', ()=>{

    test('Login button enable', async () => {
        const button = await querySelectorXPath("//*[@id=\"login\"]/div/div/div/div/div/div[4]/button", driver);
        let className = await button.getAttribute("class");
        expect(className.includes("disabled")).toBe(false);
    });

    test('Swal is show when login click', async () => {
        const button = await querySelectorXPath("//*[@id=\"login\"]/div/div/div/div/div/div[4]/button", driver);
        await button.click();
        const body = await querySelectorXPath("/html/body", driver);
        let className = await body.getAttribute("class");
        expect(className.includes("swal2-shown")).toBe(false);
    });

    test('Redirect login after last test (click on button)', async () => {
        const titlePageNav = await querySelectorId("title-page-nav", driver);
        const html = await titlePageNav.getAttribute("innerHTML");
        expect(html.includes("Gestion Stock")).toBe(true);
    });
});