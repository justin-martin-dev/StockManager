/**** RayonView test ****/

describe('RayonView test', function () {
    test('Test if nb article in first rayon is according to badge', async () => {
        /** Database part **/
        let snapshot = await FirebaseAdminAPI.getDatabase().collection('Rayon').get();
        let firstRayonData = snapshot.docs[0].data();
        let snapshotArticle = await FirebaseAdminAPI.getDatabase().collection('Article').where("rayonId", "==", firstRayonData.rayonId);
        let nbArticleFirstRayon = snapshotArticle.docs.length;

        console.log(nbArticleFirstRayon);
        /** UI part **/
        const badge = await querySelectorId(`badge-nb-${firstRayonData.rayonId}`, driver);
        let text = await badge.getText();

        expect(text).toEqual(nbArticleFirstRayon);
    })
});