const { By, until } = require('selenium-webdriver')

const waitUntilTime = 20000;

async function querySelectorCss(selector, driver) {
    const el = await driver.wait(
        until.elementLocated(By.css(selector)),
        waitUntilTime
    );
    return await driver.wait(until.elementIsVisible(el), waitUntilTime)
}

async function querySelectorId(id, driver) {
    const el = await driver.wait(
        until.elementLocated(By.id(id)),
        waitUntilTime
    );
    return await driver.wait(until.elementIsVisible(el), waitUntilTime)
}


async function querySelectorXPath(xpath, driver) {
    const el = await driver.wait(
        until.elementLocated(By.xpath(xpath)),
        waitUntilTime
    );
    return await driver.wait(until.elementIsVisible(el), waitUntilTime)
}

module.exports = {
    querySelectorCss,
    querySelectorId,
    querySelectorXPath
};