const admin = require('firebase-admin');
const serviceAccount = require('../conf/stockmanager-test-serviceAccountKey.json');

class FirebaseAdminAPI {
    static firebaseInstance;
    static databaseInstance;
    static authInstance;

    static init(){
        FirebaseAdminAPI.firebaseInstance = admin.initializeApp({
            credential: admin.credential.cert(serviceAccount),
            databaseURL: "https://stockmanager-test.firebaseio.com"
        });
        FirebaseAdminAPI.databaseInstance = FirebaseAdminAPI.firebaseInstance.firestore();
        FirebaseAdminAPI.authInstance = FirebaseAdminAPI.firebaseInstance.auth()
    }
    static getDatabase(){return FirebaseAdminAPI.databaseInstance}
    static getAuth(){return FirebaseAdminAPI.authInstance}
}

module.exports = FirebaseAdminAPI;