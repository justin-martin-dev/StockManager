import Article from '../../../model/Article';

it('Construct with json value', () => {
    const articleJson = {
        articleId: "46219550-1750-11ea-91ec-b3047519c5f9",
        nom: "chaussure",
        prix: 70,
        quantite: 50,
        tva: 13,
        rayonId: "46219550-1750-11ea-91ec-b3047519c5f9"
    };
    const articleParsed = Article.fromData(articleJson);

    expect({
        articleId: articleParsed.articleId,
        nom: articleParsed.nom,
        prix: articleParsed.prix,
        quantite: articleParsed.quantite,
        tva: articleParsed.tva,
        rayonId: articleParsed.rayonId
    }).toEqual(articleJson);
});