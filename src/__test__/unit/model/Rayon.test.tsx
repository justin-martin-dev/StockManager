import Article from '../../../model/Article';
import Rayon from "../../../model/Rayon";

it('Construct with json value', () => {
    const rayonJson = {
        rayonId: "70c0c2f0-223a-11ea-97b9-5541edf99c2c",
        nom: "Box",
    };
    const rayonParsed = Rayon.fromData(rayonJson);

    expect({
        rayonId: rayonParsed.rayonId,
        nom: rayonParsed.nom
    }).toEqual(rayonJson);
});