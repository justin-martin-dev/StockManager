import Article from '../../../model/Article';
import Rayon from "../../../model/Rayon";
import User from "../../../model/User";

it('Construct with json value', () => {
    const userJson = {
        userId: "KqsvW484cGdhlrPj52aG",
        nom: "Martin",
        prenom: "Justin",
        email: "justin.martin@toolpad.fr",
        isDirector: false,
        rayonId: "oe0j5POPI9IbSWnD0ZfK"
    };
    const userParsed = User.fromData(userJson);

    expect({
        userId: userParsed.userId,
        nom: userParsed.nom,
        prenom: userParsed.prenom,
        email: userParsed.email,
        isDirector: userParsed.isDirector,
        rayonId: userParsed.rayonId,
    }).toEqual(userJson);
});