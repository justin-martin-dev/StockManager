import Format from "../../../utils/Format";

describe('Test email format IsEmail', ()=>{
    it('Test email company with well-format', () => {
        const emailValid = "test@company.fr";
        expect(Format.isEmail(emailValid)).toEqual(true);
    });


    it('Test email company with bad format', () => {
        const emailInvalid = "testsomethingrRandom.fr";
        expect(Format.isEmail(emailInvalid)).toEqual(false);
    });
});

