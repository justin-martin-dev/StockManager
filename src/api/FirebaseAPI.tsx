import * as firebase from "firebase/app"
import "firebase/auth";
import "firebase/firestore";
import "firebase/functions";
import "firebase/storage";
import User from "../model/User";
let firebaseConfig:any;

if(process.env.FIREBASE_ENV==="test") {
    firebaseConfig = require("./test.firebase.json");
} else {
    firebaseConfig = require("./production.firebase.json");
}


class FirebaseAPI {
    static firebaseInstance: firebase.app.App;
    static databaseInstance: firebase.firestore.Firestore;
    static authInstance: firebase.auth.Auth;
    static functionInstance: firebase.functions.Functions;
    static storageInstance: firebase.storage.Storage;
    static user:User;

    static init(){
        FirebaseAPI.firebaseInstance = firebase.initializeApp(firebaseConfig);
        FirebaseAPI.databaseInstance = FirebaseAPI.firebaseInstance.firestore();
        FirebaseAPI.authInstance = FirebaseAPI.firebaseInstance.auth();
        FirebaseAPI.functionInstance = FirebaseAPI.firebaseInstance.functions();

        FirebaseAPI.getDatabase().enablePersistence({
            synchronizeTabs:true
        });
        FirebaseAPI.getAuth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
        .catch((error:any)=>{
           console.log("error", error);
        });
    }

    static getDatabase():firebase.firestore.Firestore{return FirebaseAPI.databaseInstance}
    static getAuth():firebase.auth.Auth{return FirebaseAPI.authInstance}
    static getFunctions():firebase.functions.Functions{return FirebaseAPI.functionInstance}

    static setUserConnected(user: User){
        FirebaseAPI.user = user;
    }

    static getUser():User {
        return FirebaseAPI.user;
    }
}
export default FirebaseAPI