import * as uuid from "uuid";

class ToolPad {
    constructor(url, callback) {
        this.listenerCallback = {};
        this.socket = new WebSocket(url);
        this.socket.onopen = function () {
            callback()
        };

        this.socket.onmessage = this.onMessage
    }

    onMessage = (event) => {
        const data = JSON.parse(event.data);
        if(data.commandName === "response-click"){
            console.log("listener", this.listenerCallback);
            console.log("return", data.clickReturnValue);
            this.listenerCallback[data.clickReturnValue]();
        }
    };

    setInterface = ({interfaceType, title}) => {
        let request = {
            commandName: "setInterface",
            typeInterface: interfaceType,
            title: title
        };
        this.socket.send(JSON.stringify(request));
    };

    setButton = ({buttonId, text, color, icon, callback}) => {
        const key = uuid.v1();
        let request = {
            commandName: "setButton",
            buttonId: buttonId,
            buttonText: text,
            color: color,
            buttonIcon: icon,
            clickKeyValue: key
        };
        this.listenerCallback[key] = callback;
        this.socket.send(JSON.stringify(request));
    };
}

class ToolPadSingleton {
    static toolpad;

    static getInstance(){
        return ToolPadSingleton.toolpad;
    }

    static init(url, callback){
        ToolPadSingleton.toolpad = new ToolPad(url, callback);
    }

}
export default ToolPadSingleton;