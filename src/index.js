/** Entry file **/
import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom';

/** Importation des ressources de styles **/
import './assets/css/index.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';

/** Importation du composant ReactJs principale **/
import App from './App';
import * as serviceWorker from './serviceWorker';
import { createBrowserHistory } from 'history';

/** Importation de la classe d'échange Firebase (BDD, Authentification)**/
import FirebaseAPI from "./api/FirebaseAPI";
import ToolPadSingleton from "./api/ToolPad";

const customHistory = createBrowserHistory();

FirebaseAPI.init();
ToolPadSingleton.init("ws://localhost/ToolPad", ()=>{
    ReactDOM.render(
        <Router history={customHistory}>
            <App />
        </Router>,
        document.getElementById('root')
    );

    serviceWorker.unregister();
});

//Fonction de rendu


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA

