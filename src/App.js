import React from 'react';
/** Composants librairies **/
import {Switch, Route} from 'react-router-dom';
import {MDBCard, MDBCardBody, MDBCol, MDBContainer, MDBRow} from "mdbreact";

/** Composants toolpad **/
import RoutesWithNavigation from './components/RoutesWithNavigation';
import Login from './components/pages/Login';


/** Data function access **/
import FirebaseAPI from "./api/FirebaseAPI";
import User from "./model/User";

/** Assets **/
import gifLoading from "./assets/img/loading.gif";
import './assets/css/App.css';

class App extends React.Component {
    //Intinitalisation de l'état
    state={
        loading: true
    };

    componentDidMount() {
        /** Récupération du UserID connecté pour récupération de ses données dans la BDD **/
        FirebaseAPI.getAuth().onAuthStateChanged((firebaseUser)=> {
     //       console.log(firebaseUser);
            if(firebaseUser !== null){ //Si connecté
                FirebaseAPI.getDatabase().collection("User").doc(firebaseUser.uid) //Récupération
                    .onSnapshot((snapshot => {
                        const userData = snapshot.data();
                        const user = User.fromData(userData); //Création de l'objet et mise à jour du composants
                        FirebaseAPI.setUserConnected(user);
                        this.setState({
                            isUserConnected: true,
                            loading: false
                        })
                    }))
            } else {
                this.setState({
                    isUserConnected: false,
                    loading: false
                })
            }
        })
    }

    render() {
        const {loading} = this.state;
        if(loading){
            return (
                <MDBContainer fluid>
                    <MDBRow center={true}>
                        <MDBCol size="2" className="mt-5">
                            <MDBCard>
                                <MDBCardBody>
                                    <img src={gifLoading} alt="gif-loading"/>
                                </MDBCardBody>
                            </MDBCard>
                        </MDBCol>
                    </MDBRow>
                </MDBContainer>
            )
        } else {
            //Calling Login not connected and main connected components RouteWithNavigation
            return (
                <Switch>
                    {!this.state.isUserConnected ? (
                        <Route path='/' component={Login} />
                    ) : <RoutesWithNavigation />}
                </Switch>
            )
        }
    }
}

export default App;
