interface RayonData {
    rayonId: string;
    nom: string;
}

class Rayon {
    private _rayonId: string;
    private _nom: string;

    constructor(rayonId: string, nom: string) {
        this._rayonId = rayonId;
        this._nom = nom;
    }

    get rayonId(): string {
        return this._rayonId;
    }

    set rayonId(value: string) {
        this._rayonId = value;
    }

    get nom(): string {
        return this._nom;
    }

    set nom(value: string) {
        this._nom = value;
    }

    static fromData(rayonData: RayonData): Rayon{
        return new this(
            rayonData.rayonId,
            rayonData.nom,
        )
    }
}

export default Rayon