interface UserData {
    userId: string;
    nom: string;
    prenom: string;
    email: string;
    isDirector: boolean;
    rayonId: string | null;
}

class User {
    private _userId: string;
    private _nom: string;
    private _prenom: string;
    private _email: string;
    private _isDirector: boolean = false;
    private _rayonId: string | null = null;

    constructor(userId: string, nom: string, prenom: string, email: string, isDirector: boolean, rayonId: string | null) {
        this._userId = userId;
        this._nom = nom;
        this._prenom = prenom;
        this._email = email;
        this._isDirector = isDirector;
        this._rayonId = rayonId;
    }

    get userId(): string {
        return this._userId;
    }

    set userId(value: string) {
        this._userId = value;
    }

    get nom(): string {
        return this._nom;
    }

    set nom(value: string) {
        this._nom = value;
    }

    get prenom(): string {
        return this._prenom;
    }

    set prenom(value: string) {
        this._prenom = value;
    }

    get email(): string {
        return this._email;
    }

    set email(value: string) {
        this._email = value;
    }

    get isDirector(): boolean {
        return this._isDirector;
    }

    set isDirector(value: boolean) {
        this._isDirector = value;
        if(!value){
            this.rayonId = null;
        }
    }

    get rayonId(): string | null {
        return this._rayonId;
    }

    set rayonId(value: string | null) {
        this._rayonId = value;
    }

    toData(): UserData {
        return {
            userId: this.userId,
            nom: this.nom,
            prenom: this.prenom,
            email: this.email,
            isDirector: this.isDirector,
            rayonId: this.isDirector ? null : this.rayonId
        };
    }

    static fromData(userData: UserData): User{
        return new User(
            userData.userId,
            userData.nom,
            userData.prenom,
            userData.email,
            userData.isDirector,
            userData.rayonId
        );
    }
}


export default User;