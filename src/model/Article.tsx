interface ArticleData {
    articleId: string;
    rayonId: string;
    nom: string;
    prix: number;
    quantite: number,
    tva: number
}

class Article {
    private _articleId: string;
    private _rayonId: string;
    private _nom: string;
    private _prix: number;
    private _quantite: number;
    private _tva: number;

    constructor(articleId: string, rayonId: string, nom: string, prix: number, quantite: number, tva: number) {
        this._articleId = articleId;
        this._rayonId = rayonId;
        this._nom = nom;
        this._prix = prix;
        this._quantite = quantite;
        this._tva = tva;
    }

    get articleId(): string {
        return this._articleId;
    }

    set articleId(value: string) {
        this._articleId = value;
    }

    get rayonId(): string {
        return this._rayonId;
    }

    set rayonId(value: string) {
        this._rayonId = value;
    }

    get nom(): string {
        return this._nom;
    }

    set nom(value: string) {
        this._nom = value;
    }

    get prix(): number {
        return this._prix;
    }

    set prix(value: number) {
        this._prix = value;
    }

    get quantite(): number {
        return this._quantite;
    }

    set quantite(value: number) {
        this._quantite = value;
        if(this._quantite < 0)
            this.quantite = 0;
    }

    get tva(): number {
        return this._tva;
    }

    set tva(value: number) {
        this._tva = value;
    }

    static fromData(articleData: ArticleData): Article {
        return new this(
            articleData.articleId,
            articleData.rayonId,
            articleData.nom,
            articleData.prix,
            articleData.quantite,
            articleData.tva
        );
    }

    toData(): ArticleData{
        return {
            articleId: this.articleId,
            nom: this.nom,
            prix: this.prix,
            quantite: this.quantite,
            rayonId: this.rayonId,
            tva: this.tva
        };
    }
}

export default Article