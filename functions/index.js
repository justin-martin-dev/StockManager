const functions = require('firebase-functions');
const admin = require('firebase-admin');

const serviceAccount = require("./stockmanager-firebase-adminsdk");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

exports.automateDeleteUser = functions.firestore
    .document("User/{userId}")
    .onDelete((snapshot, context)=>{
        const deletedValue = snapshot.data();
        return admin.auth().deleteUser(deletedValue.userId);
    });

exports.automateUpdateEmailUser = functions.firestore
    .document("User/{userId}")
    .onUpdate((snapshot)=>{
       if(snapshot.before.data().email !== snapshot.after.data().email){
           return admin.auth().updateUser(snapshot.after.data().userId, {email:snapshot.after.data().email});
       }
    });

exports.updatePasswordUser = functions.https.onCall((data => {
    return admin.auth().updateUser(data.changeValue.userId,  {
        password: data.changeValue.password
    })
}));